package controller

import (
	"assesment-5-golang-restful-api/app/models"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	"golang.org/x/crypto/bcrypt"

	"github.com/gin-gonic/gin"

	jwt "github.com/dgrijalva/jwt-go"
)

// CreateUser comment.
func (strDB *StrDB) CreateUser(c *gin.Context) {
	var (
		user   models.User
		result gin.H
	)

	err := c.Bind(&user)
	if err != nil {
		fmt.Println("tidak ada data")
	}

	hash, err := bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.DefaultCost)
	if err != nil {
		log.Println(err)
	}
	user.Password = string(hash)

	strDB.DB.Create(&user)
	result = gin.H{
		"result": user,
	}
	c.JSON(http.StatusOK, result)
}

// DeleteUser comment.
func (strDB *StrDB) DeleteUser(c *gin.Context) {
	var (
		user   models.User
		result gin.H
	)
	id := c.Param("id")
	err := strDB.DB.First(&user, id).Error
	if err != nil {
		result = gin.H{
			"result": "data not found",
		}
	}
	fmt.Println(user)
	err = strDB.DB.Delete(&user).Error
	if err != nil {
		result = gin.H{
			"result": "delete failed",
		}
	} else {
		result = gin.H{
			"result": "Data deleted successfully",
		}

	}

	c.JSON(http.StatusOK, result)
}

// UpdateUser comment.
func (strDB *StrDB) UpdateUser(c *gin.Context) {
	var (
		user    models.User
		newUser models.User
		result  gin.H
	)

	id := c.Param("id")
	err := c.Bind(&newUser)

	err = strDB.DB.First(&user, id).Error
	if err != nil {
		result = gin.H{
			"result": "data not found",
		}
	}

	err = strDB.DB.Model(&user).Updates(newUser).Error
	if err != nil {
		result = gin.H{
			"result": "update failed",
		}
	} else {
		result = gin.H{
			"result": "update succesful",
		}
	}
	c.JSON(http.StatusOK, result)
}

// GetUser comment.
func (strDB *StrDB) GetUser(c *gin.Context) {

	var (
		user   []models.User
		result gin.H
	)

	strDB.DB.Select("full_name").Find(&user)
	if len(user) <= 0 {
		result = gin.H{
			"result": nil,
			"count":  0,
		}
	} else {
		result = responseAPI(user, len(user))
	}
	c.JSON(http.StatusOK, result)
}

// GetOneUser comment.
func (strDB *StrDB) GetOneUser(c *gin.Context) {
	var (
		user   []models.User
		result gin.H
	)

	id := c.Param("id")
	strDB.DB.First(&user, id)
	if len(user) <= 0 {
		result = gin.H{
			"result": nil,
		}
	} else {
		result = gin.H{
			"result": user,
		}
	}

	c.JSON(http.StatusOK, result)
}

// GetSearchUser comment.
func (strDB *StrDB) GetSearchUser(c *gin.Context) {
	var (
		user   []models.User
		result gin.H
	)

	name := c.DefaultQuery("full_name", "")
	email := c.DefaultQuery("email", "")
	office := c.DefaultQuery("office_id", "")
	strDB.DB.Where("full_name LIKE ? OR email LIKE ? OR office_id LIKE ?", name, email, office).Find(&user)
	if len(user) <= 0 {
		result = gin.H{
			"result": nil,
		}
	} else {
		result = responseAPI(user, len(user))
	}

	c.JSON(http.StatusOK, result)
}

//LoginUser comment.
func (strDB *StrDB) LoginUser(c *gin.Context) {
	var (
		user   models.User
		userDB models.User
		result gin.H
	)

	errData := c.Bind(&user)
	if errData != nil {
		fmt.Println("tidak ada data")
	}

	strDB.DB.Where("email = ?", user.Email).First(&userDB)
	errPass := bcrypt.CompareHashAndPassword([]byte(userDB.Password), []byte(user.Password))
	if errPass != nil {
		fmt.Println(errPass)
		result = gin.H{
			"result": "password salah",
		}
	} else {
		type authCustomClaims struct {
			Email string `json:"email"`
			jwt.StandardClaims
		}
		claims := &authCustomClaims{
			user.Email,
			jwt.StandardClaims{
				ExpiresAt: time.Now().Add(time.Hour * 48).Unix(),
				IssuedAt:  time.Now().Unix(),
			},
		}
		sign := jwt.NewWithClaims(jwt.GetSigningMethod("HS256"), claims)
		token, err := sign.SignedString([]byte(os.Getenv("SECRET_TOKEN")))

		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{
				"message": err.Error(),
			})
			c.Abort()
		}
		result = gin.H{
			"result": token,
		}
	}
	c.JSON(http.StatusOK, result)
}

//UserOffice comment.
func (strDB *StrDB) UserOffice(c *gin.Context) {
	var (
		user []models.User

		result gin.H
	)

	strDB.DB.Joins("Office").Find(&user)
	if len(user) <= 0 {
		result = gin.H{
			"result":  nil,
			"count":   0,
			"message": "Data not found",
		}
	} else {
		result = responseAPI(user, len(user))
	}

	c.JSON(http.StatusOK, result)
}

//GetUserOffice comment.
func (strDB *StrDB) GetUserOffice(c *gin.Context) {
	var (
		user   []models.User
		result gin.H
	)

	id := c.Param("id")
	strDB.DB.Joins("Office").First(&user, id)
	if len(user) <= 0 {
		result = gin.H{
			"result": nil,
		}
	} else {
		result = gin.H{
			"result": user,
		}
	}

	c.JSON(http.StatusOK, result)
}

//UserTodoList comment.
func (strDB *StrDB) UserTodoList(c *gin.Context) {
	var (
		todo   []models.Todos
		result gin.H
	)

	id := c.Param("id")
	strDB.DB.Joins("JOIN users ON users.id = todos.user_id").Where("todos.user_id = ?", id).Find(&todo)
	if len(todo) <= 0 {
		result = gin.H{
			"result":  nil,
			"message": "data not found",
		}
	} else {
		result = responseAPI(todo, len(todo))
	}

	c.JSON(http.StatusOK, result)
}
