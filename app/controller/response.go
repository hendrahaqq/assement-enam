package controller

import (
	"math"
	"strconv"

	"github.com/gin-gonic/gin"
)

func responseAPI(str interface{}, count int) gin.H {
	return gin.H{
		"message": "success",
		"result":  str,
		"count":   count,
	}
}

func responsePagination(str interface{}, perPage float32, total int64, currentPage float32, pathStaging string, endPoint string) gin.H {

	lastPage, from, to := calculatePage(total, perPage, currentPage)

	stringLastPage := strconv.Itoa(int(lastPage))

	var (
		prevPageURL, nextPageURL string
	)

	if int32(currentPage) == 1 {
		prevPageURL = ""
	} else {
		prevPage := int(currentPage - 1)
		prevPageURL = strconv.Itoa(prevPage)
	}

	if int32(currentPage) == lastPage {
		nextPageURL = ""
	} else {
		nextPage := int(currentPage + 1)
		nextPageURL = strconv.Itoa(nextPage)
	}

	return gin.H{
		"status":         "success",
		"total":          total,
		"per_page":       perPage,
		"current_page":   currentPage,
		"last_page":      lastPage,
		"from":           from,
		"to":             to,
		"first_page_url": pathStaging + endPoint,
		"last_page_url":  pathStaging + endPoint + stringLastPage,
		"next_page_url":  pathStaging + endPoint + nextPageURL + strconv.Itoa(int(perPage)),
		"prev_page_url":  pathStaging + endPoint + prevPageURL + strconv.Itoa(int(perPage)),
		"path":           pathStaging,
		"data":           str,
	}
}

func calculatePage(total int64, perPage float32, currentPage float32) (int32, int32, int32) {
	var (
		lastPage float64
		from     float32
		to       float32
	)

	lastPage = math.Ceil(float64(total) / float64(perPage))
	from = ((currentPage - 1) * perPage) + 1
	to = from + perPage - 1

	return int32(lastPage), int32(from), int32(to)

}
