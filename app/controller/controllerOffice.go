package controller

import (
	"assesment-5-golang-restful-api/app/models"
	"fmt"
	"net/http"
	"os"
	"strconv"

	"github.com/gin-gonic/gin"
)

// CreateOffice comment.
func (strDB *StrDB) CreateOffice(c *gin.Context) {
	var (
		office models.Office
		result gin.H
	)

	err := c.Bind(&office)
	if err != nil {
		fmt.Println("tidak ada data")
	}
	strDB.DB.Create(&office)
	result = gin.H{
		"result": office,
	}
	c.JSON(http.StatusOK, result)
}

// DeleteOffice comment.
func (strDB *StrDB) DeleteOffice(c *gin.Context) {
	var (
		office models.Office
		result gin.H
	)
	id := c.Param("id")
	err := strDB.DB.First(&office, id).Error
	if err != nil {
		result = gin.H{
			"result": "data not found",
		}
	}
	fmt.Println(office)
	err = strDB.DB.Delete(&office).Error
	if err != nil {
		result = gin.H{
			"result": "delete failed",
		}
	} else {
		result = gin.H{
			"result": "Data deleted successfully",
		}

	}

	c.JSON(http.StatusOK, result)
}

// UpdateOffice comment.
func (strDB *StrDB) UpdateOffice(c *gin.Context) {
	var (
		office    models.Office
		newOffice models.Office
		result    gin.H
	)

	id := c.Param("id")
	err := c.Bind(&newOffice)

	err = strDB.DB.First(&office, id).Error
	if err != nil {
		result = gin.H{
			"result": "data not found",
		}
	}

	err = strDB.DB.Model(&office).Updates(newOffice).Error
	if err != nil {
		result = gin.H{
			"result": "update failed",
		}
	} else {
		result = gin.H{
			"result": "update succesful",
		}
	}
	c.JSON(http.StatusOK, result)
}

// GetOffice comment.
func (strDB *StrDB) GetOffice(c *gin.Context) {
	var (
		office []models.Office
		count  int64
		result gin.H
	)

	//path
	endpoint := "office-show/"
	pathStaging := os.Getenv("URL_STAGING")

	//current page
	current := c.Param("current")
	currentPage, _ := strconv.Atoi(current)

	//limit perpage
	limit, _ := strconv.Atoi(c.Param("perpage"))
	perPage := float32(limit)

	//count all
	strDB.DB.Table("offices").Count(&count)

	//select with limit
	strDB.DB.Limit(limit).Find(&office)

	if len(office) <= 0 {
		result = gin.H{
			"status":  "failed",
			"message": "Data Not Found",
		}
	} else {
		// result = responseAPI(office, len(office))
		result = responsePagination(office, perPage, count, float32(currentPage), pathStaging, endpoint)
	}
	c.JSON(http.StatusOK, result)
}

// GetOneOffice comment.
func (strDB *StrDB) GetOneOffice(c *gin.Context) {
	var (
		office []models.Office
		result gin.H
	)

	id := c.Param("id")
	strDB.DB.First(&office, id)
	if len(office) <= 0 {
		result = gin.H{
			"result": nil,
		}
	} else {
		result = gin.H{
			"result": office,
		}
	}

	c.JSON(http.StatusOK, result)
}

// GetSearchOffice comment.
func (strDB *StrDB) GetSearchOffice(c *gin.Context) {
	var (
		office []models.Office
		result gin.H
	)

	name := c.DefaultQuery("name", "")
	address := c.DefaultQuery("address", "")
	strDB.DB.Where("name LIKE ? OR address LIKE ?", name, address).Find(&office)
	if len(office) <= 0 {
		result = gin.H{
			"result": nil,
		}
	} else {
		result = responseAPI(office, len(office))
	}

	c.JSON(http.StatusOK, result)
}

// GetOfficeUsers comment.
func (strDB *StrDB) GetOfficeUsers(c *gin.Context) {
	var (
		user   []models.User
		result gin.H
	)

	id := c.Param("id")
	strDB.DB.Joins("JOIN offices ON offices.id = users.id").Where("users.office_id = ?", id).Find(&user)
	if len(user) <= 0 {
		result = gin.H{
			"result":  nil,
			"message": "data not found",
		}
	} else {
		result = responseAPI(user, len(user))
	}

	c.JSON(http.StatusOK, result)
}
